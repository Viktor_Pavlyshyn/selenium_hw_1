package ua.com.epam;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;


public class FirstTest {
    final String IMAGE_TAB = "//*[@id='hdtb-msb-vis']/div[2]/a";
    final String GOOGLE_TABS = "//a[@jsname='ONH4Gc'][parent::div[@class='T47uwc']]";
    final String GOOGLE_IMAGE_RESULTS = "//img[ancestor::div[@class='islrc']]";
    final String GOOGLE_IMAGE_TAGS = "//span[@class='sal6Qd']";
    final String GOOGLE_SEARCH = "//input[@name='q']";

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.google.com");
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void searchApple() {
        WebElement googleSearch = driver.findElement(By.xpath(GOOGLE_SEARCH));
        googleSearch.sendKeys("Apple");
        googleSearch.submit();

        assertEquals("Apple - Пошук Google", driver.getTitle(), "Incorrect title.");

        WebElement tabImage = wait.until((dr) -> dr.findElement(By.xpath(IMAGE_TAB)));
        tabImage.click();

        assertFalse(isWebElementPresent(GOOGLE_TABS, "Зображуння"),"The Images tab is not open.");

        List<WebElement> listImages = driver.findElements(By.xpath(GOOGLE_IMAGE_RESULTS));
        assertFalse(listImages.isEmpty(), "There is no image on the page.");

        WebElement tags = wait.until((dr) -> dr.findElement(By.xpath(GOOGLE_IMAGE_TAGS)));
        assertTrue(tags.isDisplayed(),"Tags isn't displayed.");
    }

    public boolean isWebElementPresent(String xpath, String text) {
        List<WebElement> listTabs = driver.findElements(By.xpath(xpath));
        return listTabs.stream().anyMatch(tab -> tab.getText().equals(text));
    }

    @AfterTest
    public void closeSite() {
        driver.quit();
    }
}
